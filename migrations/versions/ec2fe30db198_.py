"""

Revision ID: ec2fe30db198
Revises: 75667909c415
Create Date: 2021-11-22 12:14:02.625067

"""

# revision identifiers, used by Alembic.
revision = "ec2fe30db198"
down_revision = "75667909c415"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column(
        "vendors", sa.Column("relaxed_upload_rules", sa.Boolean(), nullable=True)
    )


def downgrade():
    op.drop_column("vendors", "relaxed_upload_rules")
