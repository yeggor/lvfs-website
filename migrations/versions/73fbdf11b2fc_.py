"""

Revision ID: 73fbdf11b2fc
Revises: 4c9df1b7b40d
Create Date: 2021-03-23 13:54:21.345193

"""

# revision identifiers, used by Alembic.
revision = '73fbdf11b2fc'
down_revision = '4c9df1b7b40d'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('firmware_events', sa.Column('kind', sa.Integer(), nullable=True))


def downgrade():
    op.drop_column('firmware_events', 'kind')
