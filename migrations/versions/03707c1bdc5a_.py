"""

Revision ID: 03707c1bdc5a
Revises: dbcc05ad85ad
Create Date: 2021-10-26 12:34:50.069068

"""

# revision identifiers, used by Alembic.
revision = "03707c1bdc5a"
down_revision = "dbcc05ad85ad"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column(
        "component_translations", sa.Column("user_id", sa.Integer(), nullable=True)
    )
    op.create_foreign_key(
        None, "component_translations", "users", ["user_id"], ["user_id"]
    )


def downgrade():
    op.drop_constraint(None, "component_translations", type_="foreignkey")
    op.drop_column("component_translations", "user_id")
