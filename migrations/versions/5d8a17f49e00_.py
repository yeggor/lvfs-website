"""

Revision ID: 5d8a17f49e00
Revises: 823cdd86e851
Create Date: 2021-06-14 10:46:06.688234

"""

# revision identifiers, used by Alembic.
revision = "5d8a17f49e00"
down_revision = "823cdd86e851"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_index(
        op.f("ix_component_shard_checksums_value"),
        "component_shard_checksums",
        ["value"],
        unique=False,
    )


def downgrade():
    op.drop_index(
        op.f("ix_component_shard_checksums_value"),
        table_name="component_shard_checksums",
    )
