"""

Revision ID: 09bb942c10ed
Revises: 1a506d8e04f9
Create Date: 2022-02-23 13:11:18.001657

"""

# revision identifiers, used by Alembic.
revision = "09bb942c10ed"
down_revision = "1a506d8e04f9"

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column(
        "firmware_revisions", sa.Column("immutable", sa.Boolean(), nullable=True)
    )


def downgrade():
    op.drop_column("firmware_revisions", "immutable")
