#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position,singleton-comparison,assigning-non-slot

import os
import html
import datetime
import uuid
import fnmatch
import hashlib
import json
from typing import Dict, List, Iterator, Optional, Any

import humanize
import iso3166
import iso639

from flask import (
    Blueprint,
    Response,
    abort,
    flash,
    g,
    make_response,
    redirect,
    render_template,
    request,
    send_from_directory,
    stream_with_context,
    url_for,
)
from flask_login import login_required, login_user, logout_user
from sqlalchemy.orm import joinedload
from sqlalchemy.orm.exc import NoResultFound

from celery.schedules import crontab

from uswid import uSwidIdentity, uSwidEntity, uSwidEntityRole, uSwidLink

from pkgversion import vercmp

from lvfs import app, db, lm, ploader, csrf, tq, auth

from lvfs.pluginloader import PluginError

from lvfs.analytics.models import AnalyticVendor
from lvfs.components.models import ComponentRequirement
from lvfs.firmware.models import Firmware, FirmwareRevision
from lvfs.tests.models import Test
from lvfs.metadata.models import Remote
from lvfs.users.models import User
from lvfs.geoip.models import Geoip
from lvfs.geoip.utils import _convert_ip_addr_to_integer
from lvfs.util import _event_log, _error_internal, _get_datestr_from_datetime
from lvfs.util import (
    _get_chart_labels_days,
    _get_client_address,
    _get_settings,
    _xml_from_markdown,
)
from lvfs.vendors.models import Vendor

from .models import Client, ClientMetric, Event
from .utils import _async_regenerate_metrics

bp_main = Blueprint("main", __name__, template_folder="templates")


@tq.on_after_finalize.connect
def setup_periodic_tasks(sender, **_):
    sender.add_periodic_task(
        crontab(hour=4, minute=0),
        _async_regenerate_metrics.s(),
    )


def _user_agent_safe_for_requirement(user_agent: str) -> bool:

    # very early versions of fwupd used 'fwupdmgr' as the user agent
    if user_agent == "fwupdmgr":
        return False

    # gnome-software/3.26.5 (Linux x86_64 4.14.0) fwupd/1.0.4
    sections = user_agent.split(" ")
    for chunk in sections:
        toks = chunk.split("/")
        if len(toks) == 2 and toks[0] == "fwupd":
            return vercmp(toks[1], "0.8.0") >= 0

    # this is a heuristic; the logic is that it's unlikely that a distro would
    # ship a very new gnome-software and a very old fwupd
    for chunk in sections:
        toks = chunk.split("/")
        if len(toks) == 2 and toks[0] == "gnome-software":
            return vercmp(toks[1], "3.26.0") >= 0

    # is is probably okay
    return True


def _user_agent_use_cdn(user_agent: str) -> bool:

    # gnome-software/3.26.5 (Linux x86_64 4.14.0) fwupd/1.0.4
    for chunk in user_agent.split(" "):
        try:
            name, version = chunk.split("/", maxsplit=1)
        except ValueError:
            continue
        if name == "fwupd":
            if version.startswith("1.5."):
                return vercmp(version, "1.5.10") >= 0
            return vercmp(version, "1.6.1") >= 0

    # assume not
    return False


# this is linked from each README, so redirect to somewhere better than 404
@bp_main.route("/downloads/")
def route_downloads():
    return redirect(url_for("devices.route_list"))


class PulpManifest:
    def __init__(self):
        self._lines: List[str] = []

    def add_file(
        self, filename: str, checksum: Optional[str] = None, size: Optional[int] = None
    ) -> None:

        if not checksum:
            with open(filename, "rb") as f:
                checksum = hashlib.sha256(f.read()).hexdigest()
        if not size:
            size = os.path.getsize(filename)
        self._lines.append(
            "{},{},{}".format(os.path.basename(filename), checksum, size)
        )

    def add_fw(self, fw: Firmware) -> None:

        # add archive
        self.add_file(
            filename=fw.revisions[0].filename,
            checksum=fw.checksum_signed_sha256,
            size=fw.mds[0].release_download_size,
        )

        # add update images
        for md in fw.mds:
            for url in [md.screenshot_url_safe, md.release_image_safe]:
                if not url:
                    continue
                fn = os.path.join(app.config["DOWNLOAD_DIR"], os.path.basename(url))
                if os.path.isfile(fn):
                    self.add_file(fn)

    def make_response(self):
        rsp = make_response("\n".join(self._lines) + "\n")
        rsp.headers["Content-Disposition"] = "attachment; filename=PULP_MANIFEST"
        rsp.mimetype = "text/plain"
        return rsp


@bp_main.route("/downloads/PULP_MANIFEST/vendor")
@bp_main.route("/downloads/PULP_MANIFEST/vendor/<int:vendor_id>")
@auth.login_required
def route_pulp_manifest_vendor(vendor_id: Optional[int] = None) -> Any:

    # optional, but useful for the admin to use
    if vendor_id:
        if g.user.check_acl("@admin"):
            try:
                vendor = (
                    db.session.query(Vendor).filter(Vendor.vendor_id == vendor_id).one()
                )
            except NoResultFound:
                return Response(
                    response="Invalid Vendor", status=404, mimetype="text/plain"
                )
        else:
            return Response(
                response="Permission Denied", status=403, mimetype="text/plain"
            )
    else:
        vendor = g.user.vendor

    manifest: PulpManifest = PulpManifest()
    download_dir = app.config["DOWNLOAD_DIR"]

    # add metadata
    if request.args.get("metadata", default=1, type=int):
        for basename in vendor.remote.basenames:
            fn = os.path.join(download_dir, basename)
            if os.path.exists(fn):
                manifest.add_file(fn)

    # add firmware in embargo
    for fw in vendor.remote.fws:
        manifest.add_fw(fw)

    # return response
    return manifest.make_response()


@bp_main.route("/downloads/PULP_MANIFEST")
def route_pulp_manifest():

    manifest: PulpManifest = PulpManifest()

    # add metadata
    if request.args.get("metadata", default=1, type=int):
        download_dir = app.config["DOWNLOAD_DIR"]
        for r in (
            db.session.query(Remote)
            .filter(Remote.is_public)
            .order_by(Remote.remote_id.asc())
        ):
            for basename in r.basenames:
                fn = os.path.join(download_dir, basename)
                if os.path.exists(fn):
                    manifest.add_file(fn)

    # add firmware in stable
    for fw in (
        db.session.query(Firmware)
        .join(Remote)
        .filter(Remote.is_public)
        .join(FirmwareRevision)
        .order_by(FirmwareRevision.filename.asc())
    ):
        manifest.add_fw(fw)

    # return response
    return manifest.make_response()


def _read_file_chunks(fn: str, chunksz: int = 0x80000) -> Iterator[bytes]:
    with open(fn, "rb") as fd:
        while 1:
            buf = fd.read(chunksz)
            if buf:
                yield buf
            else:
                break


def _send_from_directory_chunked(path: str, filename: str) -> Response:
    fullpath = os.path.join(path, filename)
    try:
        response = Response(
            stream_with_context(_read_file_chunks(fullpath)),
            headers={
                "Content-Type": "application/vnd.ms-cab-compressed",
                "Content-Disposition": "attachment; filename={}".format(filename),
                "Content-Length": str(os.path.getsize(fullpath)),
            },
        )
        response.cache_control.public = True
        response.cache_control.max_age = 14400
        return response
    except FileNotFoundError:
        return Response(
            response="file not found",
            status=404,
            mimetype="text/plain",
        )


@bp_main.route("/<path:resource>")
def serveStaticResource(resource):
    """Return a static image or resource"""

    # ban the robots that ignore robots.txt
    user_agent = request.headers.get("User-Agent")
    if user_agent:
        if user_agent.find("MJ12BOT") != -1:
            abort(403)
        if user_agent.find("ltx71") != -1:
            abort(403)
        if user_agent.find("Sogou") != -1:
            abort(403)

    # firmware transparency log
    if resource.startswith("ftlog"):
        return send_from_directory(app.config["DOWNLOAD_DIR"], resource)

    # log certain kinds of files
    if resource.endswith(".cab"):

        # increment the firmware download counter
        fw = (
            db.session.query(Firmware)
            .join(FirmwareRevision)
            .filter(FirmwareRevision.filename == os.path.basename(resource))
            .options(joinedload("limits"))
            .options(joinedload("vendor"))
            .first()
        )
        if not fw:
            abort(404)

        # used as a CDN key to ensure we cache the right 'version' for the file
        # from the CDN, matching the current version
        fw_timestamp = int(fw.timestamp.timestamp())
        try:
            req_timestamp = int(request.args["ts"])
            if req_timestamp != fw_timestamp:
                return Response(
                    response="firmware timestamp did not match latest signed "
                    "version (expected {}, got {}), try refreshing metadata".format(
                        fw_timestamp, req_timestamp
                    ),
                    status=404,
                    mimetype="text/plain",
                )
            return _send_from_directory_chunked(
                app.config["DOWNLOAD_DIR"], os.path.basename(resource)
            )
        except ValueError as _:
            return Response(
                response="firmware signing timestamp was not valid",
                status=404,
                mimetype="text/plain",
            )
        except KeyError:
            pass

        # check the user agent isn't in the blocklist for this firmware
        for md in fw.mds:
            req = (
                db.session.query(ComponentRequirement)
                .filter(ComponentRequirement.component_id == md.component_id)
                .filter(ComponentRequirement.kind == "id")
                .filter(ComponentRequirement.value == "org.freedesktop.fwupd")
                .first()
            )
            if req and user_agent and not _user_agent_safe_for_requirement(user_agent):
                return Response(
                    response="detected fwupd version too old",
                    status=412,
                    mimetype="text/plain",
                )

        # get the country code
        ip_val = _convert_ip_addr_to_integer(_get_client_address())
        try:
            (country_code,) = (
                db.session.query(Geoip.country_code)
                .filter(Geoip.addr_start < ip_val)
                .filter(Geoip.addr_end > ip_val)
                .first()
            )
        except TypeError:
            country_code = None

        # check the firmware vendor has no country block
        if fw.banned_country_codes:
            banned_country_codes = fw.banned_country_codes.split(",")
            try:
                if country_code in banned_country_codes:
                    return Response(
                        response="firmware not available from this IP range [{}]".format(
                            country_code
                        ),
                        status=451,
                        mimetype="text/plain",
                    )
            except TypeError:
                pass

        # check any firmware download limits
        for fl in fw.limits:
            if not fl.user_agent_glob or fnmatch.fnmatch(
                user_agent, fl.user_agent_glob
            ):
                datestr = _get_datestr_from_datetime(
                    datetime.date.today() - datetime.timedelta(1)
                )
                cnt = (
                    db.session.query(Client.id)
                    .filter(Client.firmware_id == fw.firmware_id)
                    .filter(Client.datestr >= datestr)
                    .count()
                )
                if cnt >= fl.value:
                    response = fl.response
                    if not response:
                        response = "Too Many Requests"
                    resp = Response(
                        response=response, status=429, mimetype="text/plain"
                    )
                    resp.headers["Retry-After"] = "86400"
                    return resp

        # log the client request
        if not fw.do_not_track:
            datestr = _get_datestr_from_datetime(datetime.datetime.utcnow())
            db.session.add(
                Client(
                    firmware_id=fw.firmware_id,
                    user_agent=user_agent,
                    country_code=country_code,
                    datestr=datestr,
                )
            )

            # this is updated best-effort, but also set in the cron job
            fw.download_cnt += 1
            metric = (
                db.session.query(ClientMetric)
                .filter(ClientMetric.key == "ClientCnt")
                .first()
            )
            if metric:
                metric.value += 1
            db.session.commit()

        # use the CDN
        if user_agent and _user_agent_use_cdn(user_agent):
            return redirect(
                os.path.join(
                    app.config["CDN_DOMAIN"],
                    "{}?ts={}".format(resource, fw_timestamp),
                ),
                code=302,
            )

        # fall back for older fwupd versions
        return _send_from_directory_chunked(
            app.config["DOWNLOAD_DIR"], os.path.basename(resource)
        )

    # non-firmware blobs
    if resource.startswith("downloads/"):
        return send_from_directory(
            app.config["DOWNLOAD_DIR"], os.path.basename(resource)
        )
    if resource.startswith("uploads/"):
        return send_from_directory(app.config["UPLOAD_DIR"], os.path.basename(resource))

    # static files served locally
    return send_from_directory(os.path.join(app.root_path, "static"), resource)


@app.context_processor
def utility_processor():
    def format_timestamp(tmp):
        if not tmp:
            return "n/a"
        return datetime.datetime.fromtimestamp(tmp).strftime("%Y-%m-%d %H:%M:%S")

    def format_humanize_naturalday(tmp):
        if not tmp:
            return "n/a"
        return humanize.naturalday(tmp)

    def format_humanize_naturaldelta(tmp):
        if not tmp:
            return "n/a"
        return humanize.naturaldelta(tmp)

    def format_humanize_naturaltime(tmp):
        if not tmp:
            return "n/a"
        return humanize.naturaltime(tmp.replace(tzinfo=None))

    def format_humanize_intchar(tmp):
        if tmp > 1000000:
            return "%.0fM" % (float(tmp) / 1000000)
        if tmp > 1000:
            return "%.0fK" % (float(tmp) / 1000)
        return tmp

    def format_timedelta_approx(tmp):
        return humanize.naturaltime(tmp).replace(" from now", "")

    def format_size(num, suffix="B"):
        if not isinstance(num, int) and not isinstance(num, int):
            return "???%s???" % num
        for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
            if abs(num) < 1024.0:
                return "%3.1f%s%s" % (num, unit, suffix)
            num /= 1024.0
        return "%.1f%s%s" % (num, "Yi", suffix)

    def format_locale(locale):
        if not locale:
            return iso639.to_name("en")
        try:
            return iso639.to_name(locale)
        except iso639.NonExistentLanguageError:
            pass
        try:
            return iso639.to_name(locale.split("_")[0])
        except iso639.NonExistentLanguageError:
            pass
        return "Unknown"
        # return "%.1f%s%s" % (num, "Yi", suffix)

    def format_iso3166(tmp):
        return iso3166.countries.get(tmp, ["Unknown!"])[0]

    def format_plugin_id(tmp):
        return ploader.get_by_id(tmp)

    def format_html_from_markdown(tmp):
        if not tmp:
            return "<p>None</p>"
        root = _xml_from_markdown(tmp)
        txt = ""
        for n in root:
            if n.tag == "p" and n.text:
                txt += "<p>" + html.escape(n.text) + "</p>"
            elif n.tag in ["ul", "ol"]:
                txt += "<ul>"
                for c in n:
                    if c.tag == "li" and c.text:
                        txt += "<li>" + html.escape(c.text) + "</li>"
                txt += "</ul>"
        return txt

    return dict(
        format_size=format_size,
        format_locale=format_locale,
        admin_email=app.config["ADMIN_EMAIL"],
        format_humanize_naturalday=format_humanize_naturalday,
        format_humanize_naturaltime=format_humanize_naturaltime,
        format_humanize_naturaldelta=format_humanize_naturaldelta,
        format_humanize_intchar=format_humanize_intchar,
        format_timedelta_approx=format_timedelta_approx,
        format_html_from_markdown=format_html_from_markdown,
        format_timestamp=format_timestamp,
        format_iso3166=format_iso3166,
        format_plugin_id=format_plugin_id,
        loader_plugins=sorted(ploader.get_all(), key=lambda x: x.name),
    )


@lm.unauthorized_handler
def unauthorized():
    msg = ""
    if request.url:
        msg += "Tried to request %s" % request.url
    if request.user_agent:
        msg += " from %s" % request.user_agent
    flash("Permission denied: {}".format(msg), "danger")
    return redirect(url_for("main.route_index"))


@app.errorhandler(401)
def errorhandler_401(msg=None):
    return render_template("error-401.html", msg=msg), 401


@bp_main.route("/")
@bp_main.route("/lvfs/")
def route_index():
    vendors_logo = (
        db.session.query(Vendor)
        .filter(Vendor.visible_on_landing)
        .order_by(Vendor.display_name)
        .limit(10)
        .all()
    )
    vendors_quote = (
        db.session.query(Vendor)
        .filter(Vendor.quote_text != None)
        .filter(Vendor.quote_text != "")
        .order_by(Vendor.display_name)
        .limit(10)
        .all()
    )
    return render_template(
        "index.html", vendors_logo=vendors_logo, vendors_quote=vendors_quote
    )


@bp_main.route("/lvfs/dashboard")
@login_required
def route_dashboard():
    settings = _get_settings()

    # get the 10 most recent firmwares
    fws = (
        db.session.query(Firmware)
        .filter(Firmware.user_id == g.user.user_id)
        .join(Remote)
        .filter(Remote.name != "deleted")
        .order_by(Firmware.timestamp.desc())
        .limit(10)
        .all()
    )

    download_cnt = 0
    devices_cnt = 0
    appstream_ids: Dict[str, Firmware] = {}
    for fw in g.user.vendor.fws:
        download_cnt += fw.download_cnt
        for md in fw.mds:
            appstream_ids[md.appstream_id] = fw
    devices_cnt = len(appstream_ids)

    # this is somewhat clunky
    data: List[int] = []
    datestr = _get_datestr_from_datetime(
        datetime.date.today() - datetime.timedelta(days=31)
    )
    for cnt in (
        db.session.query(AnalyticVendor.cnt)
        .filter(AnalyticVendor.vendor_id == g.user.vendor.vendor_id)
        .filter(AnalyticVendor.datestr > datestr)
        .order_by(AnalyticVendor.datestr)
    ):
        data.append(int(cnt[0]))

    return render_template(
        "dashboard.html",
        fws_recent=fws,
        devices_cnt=devices_cnt,
        download_cnt=download_cnt,
        labels_days=_get_chart_labels_days(limit=len(data))[::-1],
        data_days=data,
        server_warning=settings.get("server_warning", None),
        category="home",
    )


@bp_main.route("/lvfs/newaccount")
def route_new_account():
    """New account page for prospective vendors"""
    return redirect("https://lvfs.readthedocs.io/en/latest/apply.html", code=302)


def _create_user_for_oauth_username(username):
    """If any oauth wildcard match, create a *un-committed* User object"""

    # does this username match any globs specified by the vendor
    for v in db.session.query(Vendor).filter(
        Vendor.oauth_domain_glob != None
    ):  # pylint: disable=singleton-comparison
        for glob in v.oauth_domain_glob.split(","):
            if not fnmatch.fnmatch(username.lower(), glob):
                continue
            if v.oauth_unknown_user == "create":
                return User(username=username, vendor_id=v.vendor_id, auth_type="oauth")
            if v.oauth_unknown_user == "disabled":
                return User(username=username, vendor_id=v.vendor_id)
    return None


# unauthenticated
@bp_main.route("/lvfs/login1")
def route_login1():
    if hasattr(g, "user") and g.user:
        flash("You are already logged in", "warning")
        return redirect(url_for("main.route_dashboard"))
    return render_template("login1.html")


# unauthenticated
@bp_main.route("/lvfs/login1", methods=["POST"])
def route_login1_response():
    if "username" not in request.form:
        flash("Username not specified", "warning")
        return redirect(url_for("main.route_login1"))
    username = request.form["username"].lower()
    user = db.session.query(User).filter(User.username == username).first()
    if not user:
        flash("Failed to log in: Incorrect username {}".format(username), "danger")
        return redirect(url_for("main.route_login1"))
    return render_template("login2.html", u=user)


@bp_main.route("/lvfs/login", methods=["POST"])
@csrf.exempt
def route_login():
    """A login screen to allow access to the LVFS main page"""
    if "username" not in request.form:
        flash("Username not specified", "warning")
        return redirect(url_for("main.route_login1"))

    # auth check
    username = request.form["username"].lower()
    user = db.session.query(User).filter(User.username == username).first()
    if user:
        if user.auth_type == "oauth":
            flash(
                "Failed to log in as %s: Only OAuth can be used for this user"
                % user.username,
                "danger",
            )
            return redirect(url_for("main.route_index"))
        if not user.verify_password(request.form["password"]):
            flash(
                "Failed to log in: Incorrect password for {}".format(username), "danger"
            )
            return redirect(url_for("main.route_login1"))
    else:
        # check OAuth, user is NOT added to the database
        user = _create_user_for_oauth_username(username)
        if not user:
            flash("Failed to log in: Incorrect username {}".format(username), "danger")
            return redirect(url_for("main.route_index"))
        flash("Failed to log in: {} must use OAuth to login".format(username), "danger")
        return redirect(url_for("main.route_index"))

    # check auth type
    if not user.auth_type or user.auth_type == "disabled":
        if user.dtime:
            flash(
                "Failed to log in as %s: User account was disabled on %s"
                % (username, user.dtime.strftime("%Y-%m-%d")),
                "danger",
            )
        else:
            flash(
                "Failed to log in as %s: User account is disabled" % username, "danger"
            )
        return redirect(url_for("main.route_index"))

    # check OTP
    if user.is_otp_enabled:
        if "otp" not in request.form or not request.form["otp"]:
            flash("Failed to log in: 2FA OTP required", "danger")
            return redirect(url_for("main.route_login1"))
        if not user.verify_totp(request.form["otp"]):
            flash("Failed to log in: Incorrect 2FA OTP", "danger")
            return redirect(url_for("main.route_login1"))

    # success
    login_user(user, remember=False)
    g.user = user
    if user.password_ts:
        flash("Logged in", "info")
    else:
        flash("Logged in, now change your password using Profile ⇒ User", "info")

    # if we warned the user, clear that timer
    user.unused_notify_ts = None

    # set the access time
    user.atime = datetime.datetime.utcnow()
    db.session.commit()

    return redirect(url_for("main.route_dashboard"))


@bp_main.route("/lvfs/login/<plugin_id>")
def route_login_oauth(plugin_id):

    # find the plugin that can authenticate us
    p = ploader.get_by_id(plugin_id)
    if not p:
        return _error_internal("no plugin {}".format(plugin_id))
    if not p.oauth_authorize:
        return _error_internal("no oauth support in plugin {}".format(plugin_id))
    try:
        return p.oauth_authorize(
            url_for(
                "main.route_login_oauth_authorized",
                plugin_id=plugin_id,
                _external=True,
                _scheme="https",
            )
        )
    except PluginError as e:
        return _error_internal(str(e))


@bp_main.route("/lvfs/login/authorized/<plugin_id>")
def route_login_oauth_authorized(plugin_id):

    # find the plugin that can authenticate us
    p = ploader.get_by_id(plugin_id)
    if not p:
        _error_internal("no plugin {}".format(plugin_id))
    try:
        data = p.oauth_get_data()
        if "userPrincipalName" not in data:
            return _error_internal("No userPrincipalName in profile")
    except PluginError as e:
        return _error_internal(str(e))
    except NotImplementedError:
        return _error_internal("no oauth support in plugin {}".format(plugin_id))

    # auth check
    created_account = False
    username = data["userPrincipalName"].lower()
    user = db.session.query(User).filter(User.username == username).first()
    if not user:
        user = _create_user_for_oauth_username(username)
        if user:
            db.session.add(user)
            db.session.commit()
            _event_log(
                "Auto created user of type %s for vendor %s"
                % (user.auth_type, user.vendor.group_id)
            )
            created_account = True
    if not user:
        flash("Failed to log in: no user for {}".format(username), "danger")
        return redirect(url_for("main.route_index"))
    if not user.auth_type:
        flash("Failed to log in: User account %s is disabled" % user.username, "danger")
        return redirect(url_for("main.route_index"))
    if user.auth_type != "oauth":
        flash("Failed to log in: Only some accounts can log in using OAuth", "danger")
        return redirect(url_for("main.route_index"))

    # sync the display name
    if "displayName" in data:
        if user.display_name != data["displayName"]:
            user.display_name = data["displayName"]
            db.session.commit()

    # success
    login_user(user, remember=False)
    g.user = user
    if created_account:
        flash("Logged in, and created account", "info")
    else:
        flash("Logged in", "info")

    # set the access time
    user.atime = datetime.datetime.utcnow()
    db.session.commit()

    return redirect(url_for("main.route_dashboard"))


@bp_main.route("/lvfs/logout")
@login_required
def route_logout():
    flash("Logged out from %s" % g.user.username, "info")
    ploader.oauth_logout()
    logout_user()
    return redirect(url_for("main.route_index"))


@bp_main.route("/lvfs/eventlog")
@bp_main.route("/lvfs/eventlog/<int:start>")
@bp_main.route("/lvfs/eventlog/<int:start>/<int:length>")
@login_required
def route_eventlog(start=0, length=20):
    """
    Show an event log of user actions.
    """
    # security check
    if not g.user.check_acl("@view-eventlog"):
        flash("Permission denied: Unable to show event log for non-QA user", "danger")
        return redirect(url_for("main.route_dashboard"))

    # get the page selection correct
    if g.user.check_acl("@admin"):
        eventlog_len = db.session.query(Event.id).count()
    else:
        eventlog_len = (
            db.session.query(Event.id)
            .filter(Event.vendor_id == g.user.vendor_id)
            .count()
        )

    # limit this to keep the UI sane
    if eventlog_len / length > 20:
        eventlog_len = length * 20

    # table contents
    if g.user.check_acl("@admin"):
        events = (
            db.session.query(Event)
            .order_by(Event.id.desc())
            .offset(start)
            .limit(length)
            .all()
        )
    else:
        events = (
            db.session.query(Event)
            .filter(Event.vendor_id == g.user.vendor_id)
            .order_by(Event.id.desc())
            .offset(start)
            .limit(length)
            .all()
        )
    return render_template(
        "eventlog.html",
        events=events,
        category="home",
        start=start,
        page_length=length,
        total_length=eventlog_len,
    )


@bp_main.route("/lvfs/profile")
@login_required
def route_profile():
    """
    Allows the normal user to change details about the account,
    """
    return render_template("profile.html", u=g.user)


@bp_main.route("/lvfs/guid", methods=["GET", "POST"])
def route_guid():
    """
    Allows the normal user to convert the instance ID to a GUID.
    """

    # no payload
    if request.method != "POST":
        return render_template("guid.html", instance_id=None, guid=None)

    # sanity check
    if "instance_id" not in request.form:
        flash("Input not specified", "warning")
        return redirect(url_for("main.route_guid"))

    # remove the square brackets if the user is pasting from a quirk file
    instance_id = request.form["instance_id"]
    if instance_id.startswith("["):
        instance_id = instance_id[1:]
    if instance_id.endswith("]"):
        instance_id = instance_id[:-1]

    # success
    guid = uuid.uuid5(uuid.NAMESPACE_DNS, instance_id)
    return render_template("guid.html", instance_id=instance_id, guid=guid)


@bp_main.route("/lvfs/uswid", methods=["GET", "POST"])
def route_uswid():
    """
    Allows the normal user to generate a uSWID blob.
    """

    # no payload
    if request.method != "POST":
        return render_template("uswid.html", data={"uswid": None})

    # required data
    if "format" not in request.form:
        flash("Format not specified", "warning")
        return redirect(url_for("main.route_uswid"))

    # identity
    data = {}
    identity = uSwidIdentity()
    for key in [
        "tag_id",
        "tag_version",
        "software_name",
        "software_version",
        "product",
        "summary",
        "colloquial_version",
        "revision",
        "edition",
    ]:
        if key in request.form and request.form[key]:
            data[key] = request.form[key]
            setattr(identity, key, data[key])

    # entity
    entity = uSwidEntity()
    for key in [
        "entity_name",
        "entity_regid",
    ]:
        if key in request.form and request.form[key]:
            data[key] = request.form[key]
            setattr(entity, key[7:], data[key])
    identity.add_entity(entity)

    # entity role
    entity.roles.append(uSwidEntityRole.TAG_CREATOR)
    for key, role in [
        ("software_creator", uSwidEntityRole.SOFTWARE_CREATOR),
        ("aggregator", uSwidEntityRole.AGGREGATOR),
        ("distributor", uSwidEntityRole.DISTRIBUTOR),
        ("licensor", uSwidEntityRole.LICENSOR),
        ("maintainer", uSwidEntityRole.MAINTAINER),
    ]:
        if key in request.form and request.form[key]:
            data[key] = request.form[key]
            entity.roles.append(role)

    # link
    if "link_href" in request.form and request.form["link_href"]:
        data["link_href"] = request.form["link_href"]
        identity.add_link(uSwidLink(rel="license", href=data["link_href"]))

    # export
    if request.form["format"] == "uswid":
        resp = Response(
            response=identity.export_bytes(use_header=True),
            status=200,
            mimetype="application/octet-stream",
        )
        resp.headers["Content-Disposition"] = "attachment; filename=sbom.uswid"
        return resp
    if request.form["format"] == "coswid":
        resp = Response(
            response=identity.export_bytes(),
            status=200,
            mimetype="application/octet-stream",
        )
        resp.headers["Content-Disposition"] = "attachment; filename=sbom.coswid"
        return resp
    if request.form["format"] == "ini":
        # inline ini
        data["ini"] = identity.export_ini()

    # fallthrough
    return render_template("uswid.html", data=data)


@bp_main.route("/lvfs/metrics")
def route_metrics():

    item: Dict[str, int] = {}
    for metric in db.session.query(ClientMetric).order_by(ClientMetric.key):
        item[metric.key] = metric.value

    # unsigned files
    fws = (
        db.session.query(Firmware.firmware_id)
        .filter(Firmware.signed_timestamp == None)
        .order_by(Firmware.firmware_id.asc())
        .all()
    )
    item["UnsignedFirmware"] = len(fws)

    # pending tests
    tests = (
        db.session.query(Test.test_id)
        .filter(Test.started_ts == None)
        .order_by(Test.test_id.asc())
        .all()
    )
    item["PendingTests"] = len(tests)

    dat = json.dumps(item, indent=4, separators=(",", ": "))
    return Response(response=dat, status=200, mimetype="application/json")


# old names used on the static site
@bp_main.route("/users.html")
def route_users_html():
    return redirect(url_for("docs.route_users"), code=302)


@bp_main.route("/vendors.html")
def route_vendors_html():
    return redirect(url_for("docs.route_vendors"), code=302)


@bp_main.route("/developers.html")
def route_developers_html():
    return redirect(url_for("docs.route_developers"), code=302)


@bp_main.route("/index.html")
def route_index_html():
    return redirect(url_for("main.route_index"), code=302)


@bp_main.route("/lvfs/devicelist")
def route_devicelist():
    return redirect(url_for("devices.route_list"), code=302)


@bp_main.route("/status")
@bp_main.route("/vendorlist")  # deprecated
@bp_main.route("/lvfs/vendorlist")
def route_vendorlist():
    return redirect(url_for("vendors.route_list"), code=302)
